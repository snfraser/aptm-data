/**
 * 
 */
package com.apt.data;

/**
 * 
 */
public interface IFlowProfile {

	public FlowVector getFlow(double sigma, long time);
	
}

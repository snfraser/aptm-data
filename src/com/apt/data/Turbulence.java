/**
 * 
 */
package com.apt.data;

/**
 * @author SA05SF
 *
 */
public class Turbulence {

	private double xParam;
	
	private double yParam;
	
	private double zParam;

	/**
	 * @param xParam
	 * @param yParam
	 * @param zParam
	 */
	public Turbulence(double xParam, double yParam, double zParam) {
		super();
		this.xParam = xParam;
		this.yParam = yParam;
		this.zParam = zParam;
	}

	/**
	 * @return the xParam
	 */
	public double getxParam() {
		return xParam;
	}

	/**
	 * @param xParam the xParam to set
	 */
	public void setxParam(double xParam) {
		this.xParam = xParam;
	}

	/**
	 * @return the yParam
	 */
	public double getyParam() {
		return yParam;
	}

	/**
	 * @param yParam the yParam to set
	 */
	public void setyParam(double yParam) {
		this.yParam = yParam;
	}

	/**
	 * @return the zParam
	 */
	public double getzParam() {
		return zParam;
	}

	/**
	 * @param zParam the zParam to set
	 */
	public void setzParam(double zParam) {
		this.zParam = zParam;
	}

	@Override
	public String toString() {
		return String.format("Turbulence [xParam=%s, yParam=%s, zParam=%s]", xParam, yParam, zParam);
	}
	
	
	
}

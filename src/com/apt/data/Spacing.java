/**
 * 
 */
package com.apt.data;

/**
 * @author SA05SF
 *
 */
public class Spacing {

	public double dx;
	
	public double dy;

	/**
	 * @param dx
	 * @param dy
	 */
	public Spacing(double dx, double dy) {
		super();
		this.dx = dx;
		this.dy = dy;
	}

	/**
	 * @return the dx
	 */
	public double getDx() {
		return dx;
	}

	/**
	 * @param dx the dx to set
	 */
	public void setDx(double dx) {
		this.dx = dx;
	}

	/**
	 * @return the dy
	 */
	public double getDy() {
		return dy;
	}

	/**
	 * @param dy the dy to set
	 */
	public void setDy(double dy) {
		this.dy = dy;
	}

	@Override
	public String toString() {
		return String.format("Spacing [dx=%s, dy=%s]", dx, dy);
	}

	
	
}

/**
 * 
 */
package com.apt.data;

/**
 * @author SA05SF
 *
 */
public class PositionVector {

	private double x;
	
	private double y;
	
	private double z;

	/**
	 * @param x
	 * @param y
	 * @param z
	 */
	public PositionVector(double x, double y, double z) {
		super();
		this.x = x;
		this.y = y;
		this.z = z;
	}

	/**
	 * @return the x
	 */
	public double getX() {
		return x;
	}

	/**
	 * @param x the x to set
	 */
	public void setX(double x) {
		this.x = x;
	}

	/**
	 * @return the y
	 */
	public double getY() {
		return y;
	}

	/**
	 * @param y the y to set
	 */
	public void setY(double y) {
		this.y = y;
	}

	/**
	 * @return the z
	 */
	public double getZ() {
		return z;
	}

	/**
	 * @param z the z to set
	 */
	public void setZ(double z) {
		this.z = z;
	}
	
	public PositionVector add(PositionVector offset) {
		this.x += offset.x;
		this.y += offset.y;
		this.z += offset.z;
		return this;
	}
	
}

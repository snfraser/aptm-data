package com.apt.data;

public class ChemicalComponent {

	private ChemicalComponentType type;
	
	private double mass;

	/**
	 * @param name
	 * @param mass
	 */
	public ChemicalComponent(ChemicalComponentType type, double mass) {
		this.type = type;
		this.mass = mass;
	}

	/**
	 * @return the name
	 */
	public ChemicalComponentType getType() {
		return type;
	}

	/**
	 * @return the mass
	 */
	public double getMass() {
		return mass;
	}
	
	public void addMass(double deltamass) {
		this.mass += deltamass;
	}

	public void removeMass(double deltamass) {
		if (mass < deltamass)
			mass = 0.0;
		else
			mass -= deltamass;
	}
	
	public void removeAllMass() {
		this.mass = 0.0;
	}
	
	@Override
	public String toString() {
		return "ChemicalComponent [type=" + type + ", mass=" + mass + "]";
	}
	
	
	
	
}

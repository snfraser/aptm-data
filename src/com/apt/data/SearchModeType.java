package com.apt.data;

public enum SearchModeType {
	EXHAUSTIVE, WALKER, RING, INDEXING
}

/**
 * 
 */
package com.apt.data;

/**
 * @author SA05SF
 *
 */
public class FlowVector {

	private double u;
	private double v;
	private double w;
	
	
	
	
	/**
	 * @param u
	 * @param v
	 * @param w
	 */
	public FlowVector(double u, double v, double w) {
		this.u = u;
		this.v = v;
		this.w = w;
	}
	/**
	 * @return the u
	 */
	public double getU() {
		return u;
	}
	/**
	 * @param u the u to set
	 */
	public void setU(double u) {
		this.u = u;
	}
	/**
	 * @return the v
	 */
	public double getV() {
		return v;
	}
	/**
	 * @param v the v to set
	 */
	public void setV(double v) {
		this.v = v;
	}
	/**
	 * @return the w
	 */
	public double getW() {
		return w;
	}
	/**
	 * @param w the w to set
	 */
	public void setW(double w) {
		this.w = w;
	}
	
	/**
	 * Magnitude is overall 3D scalar magnitude.
	 * @return
	 */
	public double getMagnitude() {
		return Math.sqrt(u*u + v*v + w*w);
	}
	
	/**
	 * Plane magnitude is 2D projection onto XY axes.
	 * @return
	 */
	public double getPlaneMagnitude() {
		return Math.sqrt(u*u + v*v);
	}
	@Override
	public String toString() {
		return String.format("FlowVector [u=%s, v=%s, w=%s]", u, v, w);
	}
	
	
	
}

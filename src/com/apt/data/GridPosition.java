/**
 * 
 */
package com.apt.data;

/**
 * Represents a position (i,j) within a grid.
 * 
 * @author SA05SF
 *
 */
public class GridPosition {
	
	int i;
	
	int j;

	/**
	 * @param i
	 * @param j
	 */
	public GridPosition(int i, int j) {
		super();
		this.i = i;
		this.j = j;
	}

	/**
	 * @return the i
	 */
	public int getI() {
		return i;
	}

	/**
	 * @param i the i to set
	 */
	public void setI(int i) {
		this.i = i;
	}

	/**
	 * @return the j
	 */
	public int getJ() {
		return j;
	}

	/**
	 * @param j the j to set
	 */
	public void setJ(int j) {
		this.j = j;
	}

	@Override
	public String toString() {
		return String.format("GridPosition [i=%s, j=%s]", i, j);
	}
	
	

}

package com.apt.data;

public class Node {

	/** Node reference number, unique within a Mesh.*/
	public int ref;
	
	/** X coordinate [m].*/
	public double x;
	
	/** Y coordinate [m].*/
	public double y;
	
	/** Depth [m, negative downwards].*/
	public double depth;
	
	/** Node type: sea, open, land.*/
	public NodeType type;

	/**
	 * @param ref
	 * @param x
	 * @param y
	 */
	public Node(int ref, double x, double y) {
		this.ref = ref;
		this.x = x;
		this.y = y;
	}
	
	

	/**
	 * @param ref
	 * @param x
	 * @param y
	 * @param depth
	 * @param type
	 */
	public Node(int ref, double x, double y, double depth, NodeType type) {
		super();
		this.ref = ref;
		this.x = x;
		this.y = y;
		this.depth = depth;
		this.type = type;
	}



	@Override
	public String toString() {
		return String.format("Node [ref=%d, type: %s, x=%.2f, y=%.2f, d=%.2f]", ref, type.name(), x, y, depth);
	}
	
	
	
}

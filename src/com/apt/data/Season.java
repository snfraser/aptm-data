package com.apt.data;

public enum Season {

	SPRING("SPR", "Spring"), SUMMER("SUM", "Summer"), AUTUMN("AUT", "Autumn"), WINTER("WIN", "Winter"), UNKNOWN("UNK", "Unknown");
	
	private final String shortName;
	
	private final String longName;

	/**
	 * @param shortName
	 * @param longName
	 */
	private Season(String shortName, String longName) {
		this.shortName = shortName;
		this.longName = longName;
	}

	/** Returns the short name (3 character name).
	 * @return the shortName
	 */
	public String getShortName() {
		return shortName;
	}

	/** Returns the long name (currently only EN:UK name of season) capitalized at start.
	 * @return the longName
	 */
	public String getLongName() {
		return longName;
	}
	
	
}

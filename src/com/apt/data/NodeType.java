package com.apt.data;

public enum NodeType {
	
	OPEN_BOUNDARY, LAND_BOUNDARY, SEA;

}

package com.apt.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.Polygon;


public class IndexingSearchStrategy implements SearchStrategy {

	/** All the Mesh cells.*/
	List<Cell> cells;
	
	GeometryFactory f;
	
	/** Grid spacing (dx and dy). */
	double gridSize;
	double gridXMin = 0.0;
	double gridXMax = 0.0;
	double gridYMin = 0.0;
	double gridYMax = 0.0;
	int gridNx = 0;
	int gridNy = 0;
	
	/** The indexing Cartesian grid to overlay the unstructured mesh.*/
	Map<Integer, List<Cell>> grid = new HashMap<Integer, List<Cell>>();
	
	@Override
	public void prepareStrategy(TriangularMesh mesh) {
		
		System.err.printf("ISS: prepare search using mesh: %s \n", mesh);
		this.cells = mesh.getCellList();
		System.err.printf("ISS: prepare search using %d mesh cells \n", cells.size());
		f = new GeometryFactory();
		
		// figure out the average size of cells and the extent in x and y
		double cellAreaSum = 0.0;
		double xmin = 99999999.99;
		double xmax = -999999999.99;
		double ymin = 99999999.99;
		double ymax = -99999999.99;
		
		// scan tri-mesh cells to find average cell size 
		// - maybe need to look at cell nodes to find min/max values
		int nc=0;
		for (Cell c : cells) {
			nc++;
			//System.err.printf("ISS: checking cell: %d ...\n", c.ref);
			double area = calculateArea(c);
			double xc = c.xc;
			double yc = c.yc;
			if (nc % 5000 == 0)
				System.err.printf("ISS::   Area of cell: %d : (%f, %f) is %f \n", c.ref, xc, yc, area);
			cellAreaSum += area; 
			if (xc < xmin) xmin = xc;
			if (xc > xmax) xmax = xc;
			if (yc < ymin) ymin = yc;
			if (yc > ymax) ymax = yc;
		}
		System.err.printf("ISS: min/max cell centres: x: %f to %f y: %f to %f \n", xmin, xmax, ymin, ymax);
		double avCellSize = cellAreaSum/(double)cells.size();
		// mean dimension is root of area??? - this is what we will use for Cartesian grid size
		gridSize = Math.sqrt(avCellSize); 
		// pad out the edges to make sure we get the full mesh included.
		System.err.printf("ISS:: Padding out by: %f \n", gridSize);
		System.err.printf("ISS:: xmin: %f ", xmin); xmin = xmin - 2*gridSize; System.err.printf("-> %f \n", xmin);
		System.err.printf("ISS:: xmax: %f ", xmax); xmax = xmax + 2*gridSize; System.err.printf("-> %f \n", xmax);
		System.err.printf("ISS:: ymin: %f ", ymin); ymin = ymin - 2*gridSize; System.err.printf("-> %f \n", ymin);
		System.err.printf("ISS:: ymax: %f ", ymax); ymax = ymax + 2*gridSize; System.err.printf("-> %f \n", ymax);
		System.err.printf("ISS:: Average cell area from %d cells is %f, mean size: %f \n", cells.size(), avCellSize, gridSize);
	
		// reset gridSize to eg 3x or 5x original cell size
		gridSize *= 4;
		System.err.printf("ISS:: resetting gridsize to: %f \n", gridSize);
		
		int nx = (int)((xmax-xmin)/gridSize);
		int ny = (int)((ymax-ymin)/gridSize);
		System.err.printf("ISS:: Cartesian domain bounds (x,y): (%f, %f) -> (%f, %f) : %dx%d \n", xmin, ymin, xmax, ymax, nx, ny);
		
		
		// create the overlay indexing grid
		createIndexingGrid(xmin,ymin,xmax,ymax,gridSize);
		
	}

	@Override
	public Cell performSearch(double x, double y, Cell lastCell) {
		System.err.printf("ISS::ps: find cell for location: (%f, %f) \n", x, y);
		
		// first check its even in the grid...
		if (x < gridXMin || x > gridXMax || y < gridYMin || y > gridYMax) {
			System.err.printf("ISS::performSrch: cell is outside grid boundary for location: (%f, %f)\n", x, y);
			return null;
		}
		
		// work out the cell location
		int i = (int)Math.floor((x - gridXMin)/gridSize);
		int j = (int)Math.floor((y - gridYMin)/gridSize);
		System.err.printf("ISS::ps: computed overlay grid indices are: %d, %d \n", i, j);
		
		// convert to cell ref
		int cellRef = j*gridNx + i;
		System.err.printf("ISS::ps: computed overlay grid cell ref is: %d \n", cellRef);
		
		// and get the list of overlapping cells
		List<Cell> overlaps = grid.get(cellRef);
		System.err.printf("ISS::ps: overlaps map contains %d mesh cells \n", overlaps.size());
		
		int count = 0;
		// loop over mesh cells in overlaps list
		for (Cell mc : overlaps) {
			count++;
			System.err.printf("ISS::ps: testing mesh cell: %d at (%f, %f) ...\n", mc.ref, mc.xc, mc.yc);
			if (cellContains(mc, x, y)) {
				double d2c = Math.sqrt((x - mc.xc)*(x - mc.xc) + (y - mc.yc)*(y - mc.yc));
				System.err.printf("ISS::ps: found point after %d tests, distance from centroid: %f \n", count, d2c); 
			    System.err.printf("ISS::ps: cell is: %d at (%f, %f, %f) \n", mc.ref, mc.xc, mc.yc, mc.zc);
				return mc;
			}
		}
		
		return null;
	}

	@Override
	public SearchModeType getMode() {
		return SearchModeType.INDEXING;
	}

	/**
	 * Calculate the area of a cell using JTS polygon made from the node positions.
	 * @param c The cell whose area is to be calculated.
	 * @return The area of the cell.
	 */
	private double calculateArea(Cell c) {
		//System.err.println("Check are of cell: " + c.ref);
		Polygon poly = createPolygon(c);
		//System.err.println("Check are of polygon: " + poly);
		return poly.getArea();
	}
	
	/** Test if a cell contains a point.
	 * 
	 * @param c The cell to test.
	 * @param x
	 * @param y
	 * @return True if the point (x,y) is contained by the cell c
	 */
	private boolean cellContains(Cell c, double x, double y) {
		Polygon poly = createPolygon(c); // TODO: we should have a cached mapping of these
		Point point = f.createPoint(new Coordinate(x,y));
		if (poly.contains(point) || poly.touches(point))
				return true;
			return false;
	}
	
	private void createIndexingGrid(double xmin, double ymin, double xmax, double ymax, double gridSize) {
		
		long t0 = System.currentTimeMillis();
		
		this.gridXMin = xmin;
		this.gridXMax = xmax;
		this.gridYMin = ymin;
		this.gridYMax = ymax;
		System.err.printf("ISS: create indexing overlay grid...\n");
		// first create a mapping from cells by ref to polygons for intersect calculation
		System.err.printf("ISS: generating polygons for each mesh cell...\n");
		Map<Integer, Polygon> cellPolygons = new HashMap<Integer, Polygon>();
		for (Cell c : cells) {
			Polygon poly = createPolygon(c);
			cellPolygons.put(c.ref, poly);
		}
		System.err.printf("ISS: generated %d trimesh polygons \n", cellPolygons.size());
		
		this.gridNx = (int)Math.floor((xmax - xmin)/gridSize);
		this.gridNy = (int)Math.floor((ymax - ymin)/gridSize);
		
		System.err.printf("ISS: creating overlay grid: %dx%d \n", gridNx, gridNy);
		int[] countOverlaps = new int[400]; // make a histogram of the number of overlapping cells..
		int gridRef = 0; // grid cell reference
		for (int j = 0; j < gridNy; j++) {
			for (int i = 0; i < gridNx; i++) {
				// create the grid cell at (I,J), its a list of overlapping cells
				List<Cell> intersects = new ArrayList<Cell>();
				// the extent of this cell is: i*dx -> (i+1)*dx,j*dy -> (j+1)*dy
				double x0 = gridXMin + i*gridSize;
				double y0 = gridYMin + j*gridSize;
				double x1 = x0 + gridSize;
				double y1 = y0 + gridSize;
				Polygon square = createGridSquare(x0, y0, gridSize);
				// test all mesh cells against our overlay cell
				for (Cell c : cells) {
					Polygon poly = cellPolygons.get(c.ref);
					if (poly.overlaps(square)) {
						// this cell overlaps the grid square...
						intersects.add(c);
					}
				}
				// map the intersects list onto this grid cell
				grid.put(gridRef,  intersects);
				//System.err.printf("Overlay cell: %d bounds [x: %f->%f y: %f->%f] has %d overlapping mesh cells \n", gridRef, x0, x1, y0, y1, intersects.size());
				countOverlaps[intersects.size()]++; // add to histogram
				gridRef++;
				double progress = 100.0*(double)gridRef/(double)(gridNx*gridNy);
				if (gridRef % 1000 == 0)
					System.err.printf("ISS: processed %d overlay cells out of %d progress: %.2f%%\n", gridRef, gridNx*gridNy, progress);
			}
		}
		long t1 = System.currentTimeMillis();
		System.err.printf("ISS: completed creation of indexing grid in %f s \n", (double)(t1 - t0)/1000.0);
		
		System.err.printf("Histogram of overlap counts\n");
		for (int i = 0; i < 100; i++) {
			System.err.printf("C: [%4d] : %d \n", i, countOverlaps[i]);
		}
	}
	
	private Polygon createPolygon(Cell c) {
		int ncoord = c.nodes.length+1;
		Coordinate[] coords = new Coordinate[ncoord];
		int index = 0;
		for (Node n : c.nodes) {
			double x = n.x;
			double y = n.y;
			coords[index] = new Coordinate(x,y);
			if (index == 0)
				coords[ncoord-1] = new Coordinate(x,y); 
			index++;
		}
		// link the end of the polygon
		Polygon poly = f.createPolygon(coords);
		return poly;
	}
	
	private Polygon createGridSquare(double x0, double y0, double gridSize) {
		Coordinate[] bounds = new Coordinate[5];
		bounds[0] = new Coordinate(x0, y0);
		bounds[1] = new Coordinate(x0+gridSize, y0);
		bounds[2] = new Coordinate(x0+gridSize, y0+gridSize);
		bounds[3] = new Coordinate(x0, y0+gridSize);
		bounds[4] = new Coordinate(x0, y0);
		Polygon square = f.createPolygon(bounds);
		return square;
	}
	
}

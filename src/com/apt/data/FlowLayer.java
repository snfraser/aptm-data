/**
 * 
 */
package com.apt.data;

/**
 * Represents a layer within the flow associated with a cell.
 * Stores the u,v,w components at a set of discrete times.
 * 
 * The method computeFft() can be used to pre-populate a set
 * of FFT components to allow the flow at any time to be computed.
 * 
 * @author SA05SF
 *
 */
public class FlowLayer {

	double sigmaDepth;

	double[] u; // u(t)
	
	double[] v; // v(t)
	 
	double[] w; // w(t)
	
	double[] times; // set of {t}

	
	/**
	 * Create a new FlowLayer.
	 */
	public FlowLayer(double sigmaDepth) {
		this.sigmaDepth = sigmaDepth;
		// TODO setup...
	}

	
	
	/**
	 * @return the sigmaDepth
	 */
	public double getSigmaDepth() {
		return sigmaDepth;
	}



	/**
	 * @return the u
	 */
	public double[] getU() {
		return u;
	}

	/**
	 * @param u the u to set
	 */
	public void setU(double[] u) {
		this.u = u;
	}

	/**
	 * @return the v
	 */
	public double[] getV() {
		return v;
	}

	/**
	 * @param v the v to set
	 */
	public void setV(double[] v) {
		this.v = v;
	}

	/**
	 * @return the w
	 */
	public double[] getW() {
		return w;
	}

	/**
	 * @param w the w to set
	 */
	public void setW(double[] w) {
		this.w = w;
	}

	/**
	 * @return the times
	 */
	public double[] getTimes() {
		return times;
	}

	/**
	 * @param times the times to set
	 */
	public void setTimes(double[] times) {
		this.times = times;
	}
	
	/**
	 * Obtain the flow at a specified discrete time step indicated by itime.
	 * @param itime The time-step to obtain the flow for.
	 * @return The flow at times[itime] as a FlowVector.
	 */
	public FlowVector getFlowAt(int itime) {
		FlowVector flow = new FlowVector(u[itime], v[itime], w[itime]);
		return flow;
	}
	
	/**
	 * Interpolate the flow at the specified time using the discrete samples and a nominated optimizing algorithm.
	 * @param time A time TODO [s or ms??].
	 * @return
	 */
	public FlowVector interpolateFlowAt(double time) {
		// set cU_k and phiU_k are the fourier mag and phase coefficeints for u
		
		// alt: simple linear interpolation/extrapolation
		// 1. work out the 2 times either side of time time[i] = time[0]+n*dt
		double dt = times[1] - times[0];
		int it0 = (int)Math.floor((time - times[0])/dt);
		int it1 = it0 + 1;
		
		// extrapolate before t_0...
		if (it0 < 0) {
			it0 = 0;
			it1 = 1;
		}
	
		// extrapolate after t_n...
		if (it1 >= times.length) {
			it0 = times.length-2;
			it1 = it0 + 1;
		}
	
		double uu = u[it0] + (u[it1] - u[it0]) * (time - times[it0]) / dt;
		double vv = v[it0] + (v[it1] - v[it0]) * (time - times[it0]) / dt;
		double ww = 0.0;
		
		return new FlowVector(uu, vv, ww);
	
	}
	
}

/**
 * 
 */
package com.apt.data;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 */
public class Track {

	private List<PositionVector> track;

	/**
	 * 
	 */
	public Track() {
		track = new ArrayList<PositionVector>();
	}
	
	public void addPoint(PositionVector point) {
		track.add(point);
	}
	
	public List<PositionVector> getPoints() {return new ArrayList<PositionVector>(track);}
	
}

/**
 * 
 */
package com.apt.data;

/**
 * 
 */
public class PointData {

	private double x;
	
	private double y;
	
	private double data;

	/**
	 * @param x
	 * @param y
	 * @param data
	 */
	public PointData(double x, double y, double data) {
		super();
		this.x = x;
		this.y = y;
		this.data = data;
	}

	/**
	 * @return the x
	 */
	public double getX() {
		return x;
	}

	/**
	 * @return the y
	 */
	public double getY() {
		return y;
	}

	/**
	 * @return the data
	 */
	public double getData() {
		return data;
	}

	@Override
	public String toString() {
		return String.format("PointData [x=%s, y=%s, data=%s]", x, y, data);
	}
	
	
}

package com.apt.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


public class TriangularMesh implements Mesh  {
	
	private Domain domain;
	
	/** A Mapping from cellref to cells.*/
	private Map<Integer, Cell> cells;
	
	/** A mapping from noderef to Nodes.*/
	private Map<Integer, Node> nodes;

	//private int flowTimes[];
	
	private SearchModeType searchMode = SearchModeType.EXHAUSTIVE;
	
	private SearchStrategy searchStrategy;
	
	/**
	 * @param cells
	 */
	public TriangularMesh(Domain domain, Map<Integer, Cell> cells, Map<Integer, Node> nodes) {
		this.domain = domain;
		this.cells = cells;
		this.nodes = nodes;
	}
	
	public void printCells() {
		
		for (int i = 1; i <= cells.size(); i++) {
			
			Cell c = cells.get(i);
			Node[] nodes = c.nodes;
			
			System.err.printf("Cell: %d Nodes: ", i);
			for (Node n : nodes) {
				System.err.printf(" %d (%.2f, %.2f) [%d]: ", n.ref, n.x, n.y, n.type.ordinal());
			
			}
		
			System.err.printf(" CC: (%.2f, %.2f) \n", c.xc, c.yc);
			
		}
		
		
	}

	/**
	 * @return the searchMode
	 */
	public SearchModeType getSearchMode() {
		return searchMode;
	}

	/**
	 * @param searchMode the searchMode to set
	 */
	public void setSearchStrategy(SearchStrategy strategy) {
		System.err.printf("TMesh::setsrcstrategy:: %s\n", strategy);
		searchStrategy = strategy;
		this.searchStrategy.prepareStrategy(this);
		this.searchMode = searchStrategy.getMode();
	}

	/**
	 * @return the domain
	 */
	@Override
	public Domain getDomainBounds() {
		return domain;
	}

	/**
	 * @return the cells
	 */
	public Map<Integer, Cell> getCells() {
		return cells;
	}

	/**
	 * @return the nodes
	 */
	public Map<Integer, Node> getNodes() {
		return nodes;
	}
	
	public List<Node> getNodeList() {
		List<Node> nodelist = new ArrayList<Node>();
		for (Node n: nodes.values()) {
			nodelist.add(n);
		}
		return nodelist;
	}
	
	public List<Cell> getCellList() {
		List<Cell> cellist = new ArrayList<Cell>();
		for (Cell c: cells.values()) {
			cellist.add(c);
		}
		return cellist;
	}
	
	public List<Node> getBoundaryNodes() {
		
		List<Node> nodelist = new ArrayList<Node>();
		for (Node n: nodes.values()) {
			if (n.type == NodeType.LAND_BOUNDARY || n.type == NodeType.OPEN_BOUNDARY)
			nodelist.add(n);
		}
		return nodelist;
		
	}
	
	public List<Node> getOpenBoundaryNodes() {
		
		List<Node> nodelist = new ArrayList<Node>();
		for (Node n: nodes.values()) {
			if (n.type == NodeType.OPEN_BOUNDARY)
			nodelist.add(n);
		}
		return nodelist;
		
	}
	
	public List<Node> getLandBoundaryNodes() {
		
		List<Node> nodelist = new ArrayList<Node>();
		for (Node n: nodes.values()) {
			if (n.type == NodeType.LAND_BOUNDARY)
			nodelist.add(n);
		}
		return nodelist;
		
	}
	
	

	/**
	 * @return the flowTimes
	 */
	//public int[] getFlowTimes() {
	//	return flowTimes;
	//}

	/**
	 * @param flowTimes the flowTimes to set
	 */
	//public void setFlowTimes(int[] flowTimes) {
	//	this.flowTimes = flowTimes;
	//}
	
	/** Find the cell containing x,y.
	 * @param x
	 * @param y
	 * @return the cell containing x,y.
	 */
	@Override
	public Cell findCell(double x, double y, Cell lastCell) {
		// when a particle is created it has no initial last cell, must be worked out
		// for all instances...first test the lastcell, often it wont have jumped a boundary..
		if (lastCell != null && lastCell.contains(x, y))
			return lastCell;
		
		return searchStrategy.performSearch(x, y, lastCell);
		
	}
	
	
	
	/** DEFUNCT Find the cell for an x,y position using a walk from the last cell.*/
	private Cell findCellUsingWalker(double x, double y, Cell lastCell) {
		return null;
	}
	
	/** DEFUNCT Find the cell for an x,y position using an overlayed index grid.*/
	private Cell findCellUsingIndexGrid(double x, double y) {
		return null;
	}
	
	/** DEFUNCT Find the cell for an x,y position using a mapping of neighbouring cells in 1 or more rings.*/
	private Cell findCellUsingNeighbourRings(double x, double y, Cell lastCell) {
		return null;
	}

	@Override
	public String toString() {
		return String.format("TriangularMesh [domain=%s, cells=%d, nodes=%d]", domain, cells.size(), nodes.size());
				//flowTimes != null ? flowTimes.length:0);
	}

	@Override
	public List<Cell> listCells() {
		List<Cell> mycells = new ArrayList<Cell>();
		for (Cell c : cells.values()) {
			mycells.add(c);
		}
		return mycells;
	}
	
	
}

/**
 * 
 */
package com.apt.data;

/**
 * 
 */
public enum SeabedType {
	CLAY, MUD, SILT, SAND, GRAVEL, PEBBLE, COBBLE
}

/**
 * 
 */
package com.apt.data;

/**
 * 
 */
public interface IFlowTimeSeries {

	public FlowVector getFlow(long time);
	
}

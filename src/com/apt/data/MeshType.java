/**
 * 
 */
package com.apt.data;

/**
 * @author SA05SF
 *
 */
public enum MeshType {
	
	RECTANGULAR, TRIANGULAR, VORONOI;

}

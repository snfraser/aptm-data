package com.apt.data;

import java.util.Arrays;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.Point;
import org.locationtech.jts.geom.Polygon;
import org.locationtech.jts.geom.impl.CoordinateArraySequence;

/**
 * A cell contains data about an area bounded by a set of nodes which define it.
 * Sets of CellData items can be contained in a cell. eg: seabed, flow, others?
 * 
 * @author SA05SF
 *
 */
public class Cell {

	/** Cell reference. */
	public int ref;

	/** Nodes surrounding this cell. */
	public Node[] nodes;

	/** Factory used for geometri operations - created lazily in constructor.*/
	private GeometryFactory gf;

	/**
	 * A polygon representing the cell boundary for checking whether coords are
	 * contained.
	 */
	private Polygon boundary;

	/** Cell centre X. */
	public double xc;

	/** Cell centre Y. */
	public double yc;

	public double area;

	public double zc;

	/** U component of flow with time and (TODO) layer. */
	// public double u[]; // u(t)

	/** V component of flow with time and (TODO) layer. */
	// public double v[]; // v(t)

	/** The set of times for the u and v values/ */
	// public double times[];

	/**
	 * @param nodes
	 */
	public Cell(int ref, Node[] nodes) {
		this.ref = ref;
		this.nodes = nodes;
		calculateCentroid();
		calculateArea();
	}

	private void calculateCentroid() {

		for (Node n : nodes) {
			// System.err.printf(" %d (%.2f, %.2f) : ", n.ref, n.x, n.y);
			xc += n.x;
			yc += n.y;
			zc += n.depth;
		}
		xc = xc / nodes.length;
		yc = yc / nodes.length;
		zc = zc / nodes.length;
	}

	/**
	 * Determine if a point is contained in this cell. Lazily creates the bounding
	 * polygon using the surrounding nodes.
	 * 
	 * @param x The point's x coordinate.
	 * @param y The point's y coordinate.
	 * @return True if the point defined by (x,y) is within this cell's bounds.
	 */
	public boolean contains(double x, double y) {

		// lazily create GeometryFactory..
		if (gf == null) {
			gf = new GeometryFactory();
		}

		// lazily create the cell boundary polygon
		if (boundary == null) {
			boundary = createBoundary();
		}

		// the point of interest
		Coordinate c = new Coordinate(x, y);
		Point point = gf.createPoint(c);

		return boundary.contains(point) || boundary.intersects(point);

	}

	private Polygon createBoundary() {
		Coordinate[] boundaryCoordinates = new Coordinate[nodes.length + 1];
		for (int i = 0; i < nodes.length; i++) {
			boundaryCoordinates[i] = new Coordinate(nodes[i].x, nodes[i].y);
		}
		// one more the same as the first to close the polygon
		boundaryCoordinates[nodes.length] = new Coordinate(nodes[0].x, nodes[0].y);
		Polygon aboundary = gf.createPolygon(boundaryCoordinates);

		return aboundary;
	}

	// DEFUNCT DO NOT USE
	public boolean containsSimple(double x, double y) {
		double dx = x - xc;
		double dy = y - yc;
		double dd = Math.sqrt(dx * dx + dy * dy);

		Vector2D point = new Vector2D(x, y);

		// convert to Triangle2D and use the relevant method.
		Vector2D a = new Vector2D(nodes[0]);
		Vector2D b = new Vector2D(nodes[1]);
		Vector2D c = new Vector2D(nodes[2]);
		Triangle2D tri = new Triangle2D(a, b, c);
		if (tri.contains(point))
			return true;

		// if the point is on a boundary edge...
		EdgeDistancePack edp = tri.findNearestEdge(point);
		if (edp.distance < 0.01)
			return true;

		return false;

	}

	/**
	 * Calculate if and where a line from (x0,y0) to (x1,y1) intersects a boundary
	 * of this cell. Call interesectEdges(x0, y0, x1, y1) first to see if the
	 * crossing exists.
	 */
	public PositionVector intersect(double x0, double y0, double x1, double y1) {

		// lazily create GeometryFactory..
		if (gf == null) {
			gf = new GeometryFactory();
		}

		// lazily create the cell boundary polygon
		if (boundary == null) {
			boundary = createBoundary();
		}

		Coordinate c0 = new Coordinate(x0, y0);
		Coordinate c1 = new Coordinate(x1, y1);

		Coordinate[] coordinates = new Coordinate[] { c0, c1 };
		CoordinateArraySequence points = new CoordinateArraySequence(coordinates);
		LineString line = new LineString(points, gf);

		boolean crosses = boundary.intersects(line);
		// System.err.printf(" Cell::intersect(line) crossing detected");
		LineString cline = (LineString) boundary.intersection(line);
		for (int j = 0; j < cline.getNumPoints(); j++) {
			Coordinate c = cline.getCoordinateN(j);
		}

		Point p1 = cline.getStartPoint();
		Point p2 = cline.getEndPoint();

		PositionVector pv = new PositionVector(p2.getX(), p2.getY(), 0.0);
		return pv;

	}

	/**
	 * Check if a line segment (x0,y0) to (x1,y1) intersects an edge of this cell.
	 * 
	 * @param x0 X coordinate of segment start.
	 * @param y0 Y coordinate of segment start.
	 * @param x1 X coordinate of segment end.
	 * @param y1 Y coordinate of segment end.
	 * @return The type of crossing, or null if no crossing found.
	 */
	public CrossingType intersectEdges(double x0, double y0, double x1, double y1) {

		//System.err.printf("Cell:: cal intersect edge for seg: (%f, %f) -> (%f, %f) \n", x0, y0, x1, y1);
		CrossingType c = CrossingType.SEA;

		Coordinate c0 = new Coordinate(x0, y0);
		Coordinate c1 = new Coordinate(x1, y1);

		Coordinate[] coordinates = new Coordinate[] { c0, c1 };
		CoordinateArraySequence points = new CoordinateArraySequence(coordinates);
		LineString line = new LineString(points, gf);

		int crossings = 0;
		Node last = nodes[nodes.length - 1]; // last and first node
		for (Node n : nodes) {
			// edge between the 2 nodes
			Coordinate e0 = new Coordinate(last.x, last.y);
			Coordinate e1 = new Coordinate(n.x, n.y);

			Coordinate[] edge = new Coordinate[] { e0, e1 };
			CoordinateArraySequence edgepoints = new CoordinateArraySequence(edge);
			LineString eline = new LineString(edgepoints, gf);

			boolean crossEdge = eline.intersects(line);
			crossings += (crossEdge ? 1 : 0);
			// System.err.printf(" Cell::i2(line) Intersection of edge: %d %s (%f %f) and %d
			// %s (%f %f) %b\n", last.ref,
			// last.type.name(), last.x, last.y, n.ref, n.type.name(), n.x, n.y, crossEdge);

			// decide what sort of crossing it is...
			// for a land crossing expect: LL:T LS:F SL:F
			// for an open crossing expect: OO:T OS:F SO:F
			if (crossEdge) {
				if (last.type == NodeType.OPEN_BOUNDARY && n.type == NodeType.OPEN_BOUNDARY) {
					//System.err.println("Cell:: crossing appears to be an OPEN boundary crossing");
					c = CrossingType.OPEN;
				}
				if (last.type == NodeType.LAND_BOUNDARY && n.type == NodeType.LAND_BOUNDARY) {
					//System.err.println("Cell:: crossing appears to be a LAND boundary crossing");
					c = CrossingType.LAND;
				}

				// special case where land butts upto edge of open zone - count as OPEN
				if (last.type == NodeType.OPEN_BOUNDARY && n.type == NodeType.LAND_BOUNDARY
						|| last.type == NodeType.LAND_BOUNDARY && n.type == NodeType.OPEN_BOUNDARY) {
					//System.err.println("Cell:: crossing appears to be a LAND/OPEN boundary crossing");
					c = CrossingType.OPEN;
				}

			}

			last = n;
		}

		return c;

	}

	private void calculateArea() {
		// lazily create GeometryFactory..
		if (gf == null) {
			gf = new GeometryFactory();
		}

		// lazily create the cell boundary polygon
		if (boundary == null) {
			boundary = createBoundary();
		}
		area = boundary.getArea();

	}

	@Override
	public String toString() {
		StringBuilder b = new StringBuilder();
		for (Node n : nodes) {
			b.append(n.ref).append(",");
		}
		// return String.format("Cell [ref: %d, nodes: [%s], ntimes: %d, u0: %.2f, v0:
		// %.2f]", ref, b.toString(), u.length,
		// u[0], v[0]);
		return String.format("Cell [ref: %d, (%f, %f) nodes: [%s] zc: %f:", ref, xc, yc, b.toString(), zc);
	}

}

/**
 * 
 */
package com.apt.data;

/**
 * @author SA05SF
 *
 */
public class CarbonFraction {

	double labileMass;
	
	double refractoryMass;

	/**
	 * @param labileMass
	 * @param refractoryMass
	 */
	public CarbonFraction(double labileMass, double refractoryMass) {
		super();
		this.labileMass = labileMass;
		this.refractoryMass = refractoryMass;
	}

	/**
	 * @return the labileMass
	 */
	public double getLabileMass() {
		return labileMass;
	}

	/**
	 * @return the refractoryMass
	 */
	public double getRefractoryMass() {
		return refractoryMass;
	}
	
	public double getTotalMass() {
		return labileMass + refractoryMass;
	}

	@Override
	public String toString() {
		return String.format("CarbonFraction [labileMass=%s, refractoryMass=%s]", labileMass, refractoryMass);
	}
	
	
}

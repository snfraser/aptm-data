/**
 * 
 */
package com.apt.data;

/**
 * @author SA05SF
 *
 */
public enum CrossingType {

	OPEN ("open boundary"), 
	SEA ("no boundary"), 
	LAND ("land boundary");
	
    private String description;
	
	CrossingType(String description) {
		this.description  = description;
	}
}

package com.apt.data;

public class Domain {

	public double x0;
	public double x1;

	public double y0;
	public double y1;
	
	
	/**
	 * @param x0
	 * @param x1
	 * @param y0
	 * @param y1
	 */
	public Domain(double x0, double x1, double y0, double y1) {
		this.x0 = x0;
		this.x1 = x1;
		this.y0 = y0;
		this.y1 = y1;
	}

	/**
	 * 
	 */
	public Domain(Domain other) {
		this.x0 = other.x0;
		this.y0 = other.y0;
		this.x1 = other.x1;
		this.y1 = other.y1;
	}

	/**
	 * @return the x0
	 */
	public double getX0() {
		return x0;
	}

	/**
	 * @param x0 the x0 to set
	 */
	public void setX0(double x0) {
		this.x0 = x0;
	}

	/**
	 * @return the x1
	 */
	public double getX1() {
		return x1;
	}

	/**
	 * @param x1 the x1 to set
	 */
	public void setX1(double x1) {
		this.x1 = x1;
	}

	

	/**
	 * @return the y0
	 */
	public double getY0() {
		return y0;
	}

	/**
	 * @param y0 the y0 to set
	 */
	public void setY0(double y0) {
		this.y0 = y0;
	}

	/**
	 * @return the y1
	 */
	public double getY1() {
		return y1;
	}

	/**
	 * @param y1 the y1 to set
	 */
	public void setY1(double y1) {
		this.y1 = y1;
	}

	
	
	public boolean contains(double x, double y) {
		if (x < x0 || x > x1 || y < y0 || y > y1)
			return false;
		return true;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return String.format("Domain: x0: %f, x1: %f, y0: %f, y1: %f", x0, x1, y0, y1);
	}
	
	
	
	
}

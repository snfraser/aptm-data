/**
 * 
 */
package com.apt.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryCollection;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.Polygon;
import org.locationtech.jts.triangulate.VoronoiDiagramBuilder;


/**
 * @author SA05SF
 *
 */
public class VoronoiMesh implements Mesh {

	private Domain domain;
	
	private List<Coordinate> samplePoints;
	//private List<CellData> dataPoints;
	
	private Map<Integer, Cell> cells;
	
	
	/**
	 * @param domain The domain bounds for the sample area.
	 * @param samplePoints The list of points where we have sample data: z0, erosion, grainsize, stickyness?
	 */
	public VoronoiMesh(Domain domain, List<Coordinate> samplePoints) {
	// TODO:  VoronoiMesh(Domain domain, List<SeabedSample> samplePoints) SeabedSample:: x, y, z0, eorsion, grains
		this.domain = domain;
		this.samplePoints = samplePoints;
		//this.dataPoints = dataPoints;
	
		System.err.printf("Create voronoi mesh using: %d sites \n", samplePoints.size());
		
		// boundary
		Envelope env = new Envelope(domain.x0, domain.x1, domain.y0, domain.y1);
		
		VoronoiDiagramBuilder builder = new VoronoiDiagramBuilder();
		builder.setSites(samplePoints);
		builder.setClipEnvelope(env);
		
		GeometryFactory f = new GeometryFactory();
		GeometryCollection c = (GeometryCollection)builder.getDiagram(f);
		
		// clip the resulting polygons again!
		Coordinate[] clipRegion  = new Coordinate[] {
				new Coordinate(domain.x0, domain.y0), 
				new Coordinate(domain.x1, domain.y0), 
				new Coordinate(domain.x1, domain.y1), 
				new Coordinate(domain.x0, domain.y1), 
				new Coordinate(domain.x0, domain.y0)};
		Polygon clipBoundary = f.createPolygon(clipRegion);
		
		cells = new HashMap<Integer, Cell>();
		int nodeRef = 0;
		int cellRef = 0;
		
		// each polygon is a voronoi cell.
		for (int i = 0; i < c.getNumGeometries(); i++) {
			Geometry p = c.getGeometryN(i);
			// NOTE not all cells need intersect the clipping region!!!
			Geometry cellPolygon = p.intersection(clipBoundary); // OR INSIDE IT COMPLETETLY
			//System.err.printf("Polygon: %d : points: %s \n", p.getNumPoints(), p);
			//System.err.printf("Clipped: np: %d : shape: %s \n", sp.getNumPoints(), sp);
			// new cell with these nodes as boundary
			Coordinate[] vertices = cellPolygon.getCoordinates();
			Node[] nodes = new Node[vertices.length-1];
			System.err.printf("Cell: %d nvert: %d ", cellRef, vertices.length);
			// we will need to knock one off as its  probably a duplicate of first
			double dx = Math.abs(vertices[0].x - vertices[vertices.length-1].x);
			double dy = Math.abs(vertices[0].y - vertices[vertices.length-1].y);
			System.err.printf("Cell: %d dx1n: %f dy1n: %f \n", cellRef, dx, dy);
			for (int k = 0; k < vertices.length-1; k++) {
				double x = vertices[k].x;
				double y = vertices[k].y;
				nodeRef++;
				Node node = new Node(nodeRef, x, y);
				System.err.printf("  Node:: %d x: %f y: %f \n", nodeRef, x, y);
				//node.type = NodeType.sea/land/open
				//node.depth = ??
				nodes[k] = node;
			}
			cellRef++; // starts at 1
			Cell cell = new Cell(cellRef, nodes);
			cells.put(cellRef, cell);
			System.err.printf("  cell: [%d] has %d nodes\n", cellRef, cell.nodes.length);
			
			// now work out which  original sample this cell represents...
			/*int index = 0;
			for (Coordinate coord : samplePoints) {
				// checking if sample coord: <index> is inside this new cell (cellRef)
				if (cell.contains(coord.x, coord.y)) {
					// this is the original sample point - get its ref so we can associate the data
					CellData data = dataPoints.get(index);
					System.err.printf("  sample coords: %d at (%f, %f) are inside this cell \n", index, coord.x, coord.y);
					// TODO cell.setCellData(data); or setSeabedData(data cast to SeabedCellData)
					//cell.seabedData = (SeabedCellData) data;
					continue;
				}
				index++;
			}*/
			
		}
		
		System.err.printf("Completed voronoi mesh with: %d cells \n", cells.size());
		
	}

	@Override
	public Cell findCell(double x, double y, Cell lastCell) {
		for (Cell c : cells.values()) {
			if (c.contains(x, y))
			return c;
		}
		return null;
	}

	@Override
	public Domain getDomainBounds() {
		return domain;
	}

	@Override
	public List<Cell> listCells() {
		List<Cell> mycells = new ArrayList<Cell>();
		for (Cell c : cells.values()) {
			mycells.add(c);
		}
		return mycells;
	}

}

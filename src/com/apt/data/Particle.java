/**
 * 
 */
package com.apt.data;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

/**
 * A simple particle to get things going.
 * 
 * @author SA05SF
 *
 */
public class Particle {

	private String id;
	private String id2;

	// current position
	private double x;
	private double y;
	private double z;

	// start positions
	private double sx;
	private double sy;
	private double sz;

	// last/previous position
	private double lx;
	private double ly;
	private double lz;

	// landing position
	private double bedx;
	private double bedy;
	private double bedz;

	private ParticlesState state;

	private ParticleClass type;

	//private List<ParticleTrackPoint> track;

	private CompoundMass masses;

	/** Current cell where the particle is located within the bathymetry mesh. */
	private Cell currentCell;

	/** Temp: Store feed mass content if valid. */
	//private double feedMass;

	/** Creation time. */
	private long creationTime;

	/** Time of landing. */
	private long landingTime;

	/** Time lost to domain. */
	private long lostTime;

	/** Mass represented by this particle. */
	private double transportMass;
	
	/** Density of the particle [kg/m3].*/
	private double density;
	
	/** Particle diameter [m] - assumed spherical?.*/
	private double diameter;

	/**
	 * 
	 */
	public Particle() {
		id = UUID.randomUUID().toString();
		state = ParticlesState.SUSPEND;
		//track = new ArrayList<ParticleTrackPoint>();
		masses = new CompoundMass();
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the state
	 */
	public ParticlesState getState() {
		return state;
	}

	/**
	 * @return the type
	 */
	public ParticleClass getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(ParticleClass type) {
		this.type = type;
	}

	/** Called when a particle is initially located */
	public void setStartPosition(double sx, double sy, double sz) {
		this.sx = sx;
		this.sy = sy;
		this.sz = sz;
		this.x = sx;
		this.y = sy;
		this.z = sz;
	}

	/**
	 * @return the sx
	 */
	public double getSx() {
		return sx;
	}

	/**
	 * @return the sy
	 */
	public double getSy() {
		return sy;
	}

	/**
	 * @return the sz
	 */
	public double getSz() {
		return sz;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(ParticlesState state) {
		this.state = state;
	}

	/**
	 * @return the x
	 */
	public double getX() {
		return x;
	}

	/**
	 * @param x the x to set
	 */
	public void setX(double x) {
		this.x = x;
	}

	/**
	 * @return the y
	 */
	public double getY() {
		return y;
	}

	/**
	 * @param y the y to set
	 */
	public void setY(double y) {
		this.y = y;
	}

	/**
	 * @return the z
	 */
	public double getZ() {
		return z;
	}

	/**
	 * @param z the z to set
	 */
	public void setZ(double z) {
		this.z = z;
	}

	/**
	 * @return the feedMass
	 */
	//public double getFeedMass() {
	//	return feedMass;
	//}

	/**
	 * @param feedMass the feedMass to set
	 */
	//public void setFeedMass(double feedMass) {
	//	this.feedMass = feedMass;
	//}

	/**
	 * @return the creationTime
	 */
	public long getCreationTime() {
		return creationTime;
	}


	/**
	 * @param creationTime the creationTime to set
	 */
	public void setCreationTime(long creationTime) {
		this.creationTime = creationTime;
	}
	
	/**
	 * @return the density
	 */
	public double getDensity() {
		return density;
	}

	/**
	 * @param density the density to set
	 */
	public void setDensity(double density) {
		this.density = density;
	}

	/**
	 * @return the diameter
	 */
	public double getDiameter() {
		return diameter;
	}

	/**
	 * @param diameter the diameter to set
	 */
	public void setDiameter(double diameter) {
		this.diameter = diameter;
	}


	public long getTimeSinceCreation(long time) {
		return time - creationTime;
	}

	/**
	 * @return the landingTime
	 */
	public long getLandingTime() {
		return landingTime;
	}

	/**
	 * @param landingTime the landingTime to set
	 */
	public void setLandingTime(long landingTime) {
		this.landingTime = landingTime;
	}

	public long getTimeSinceLanded(long time) {
		return time - landingTime;
	}
	
	public void setLandingPosition(double bx, double by, double bz) {
		this.bedx = bx;
		this.bedy = by;
		this.bedz = bz;
	}

	/**
	 * @return the bx
	 */
	public double getBedx() {
		return bedx;
	}

	/**
	 * @return the by
	 */
	public double getBedy() {
		return bedy;
	}

	/**
	 * @return the bz
	 */
	public double getBedz() {
		return bedz;
	}
	
	

	/**
	 * @return the lx
	 */
	public double getLx() {
		return lx;
	}

	/**
	 * @return the ly
	 */
	public double getLy() {
		return ly;
	}

	/**
	 * @return the lz
	 */
	public double getLz() {
		return lz;
	}

	/**
	 * @return the lostTime
	 */
	public long getLostTime() {
		return lostTime;
	}

	/**
	 * @param lostTime the lostTime to set
	 */
	public void setLostTime(long lostTime) {
		this.lostTime = lostTime;
	}

	/**
	 * @return the transportMass
	 */
	public double getTransportMass() {
		return transportMass;
	}

	/**
	 * @param transportMass the transportMass to set
	 */
	public void setTransportMass(double transportMass) {
		this.transportMass = transportMass;
	}

	/**
	 * Update the particle's position. Records the last position and updates the
	 * track.
	 * 
	 * @param nx the new x position. Pparam ny the new y position.
	 * @param nz the new z position.
	 */
	public void updatePosition(double nx, double ny, double nz) {
		this.lx = this.x;
		this.ly = this.y;
		this.lz = this.z;
		this.x = nx;
		this.y = ny;
		this.z = nz;
		//ParticleTrackPoint p = new ParticleTrackPoint();
		//p.x = x;
		//p.y = y;
		//p.z = z;
		//track.add(p);
	}

	//public List<ParticleTrackPoint> getTrack() {
		//return track;
	//}

	public CompoundMass getMasses() {
		return masses;
	}

	/**
	 * @param e
	 * @see java.util.List#add(java.lang.Object)
	 */
	public void addChemical(ChemicalComponent e) {
		masses.addComponent(e);
	}

	public PositionVector getCurrentPosition() {
		return new PositionVector(x, y, z);
	}

	/**
	 * @return the currentBathyCell
	 */
	public Cell getCurrentCell() {
		return currentCell;
	}

	/**
	 * @param currentBathyCell the currentBathyCell to set
	 */
	public void setCurrentCell(Cell currentCell) {
		this.currentCell = currentCell;
	}
	
	/**
	 * @return the id2
	 */
	public String getId2() {
		return id2;
	}

	/**
	 * @param id2 the id2 to set
	 */
	public void setId2(String id2) {
		this.id2 = id2;
	}

	@Override
	public String toString() {
		return String.format("Particle: %s : %s : %s : (%f, %f, %f)", id, type, state, x, y, z);
	}

	//@Override
	//public int hashCode() {
		// particle numbers look like:  P%12345 or R%12345
		//return Integer.parseInt(id.substring(2));
//	}

	//@Override
	//public boolean equals(Object obj) {
		//if (! (obj instanceof Particle))
			//return false;
		//return id2.equals(((Particle)obj).getId2());
	//}
	
	

}

/**
 * 
 */
package com.apt.data.test;

import java.util.ArrayList;
import java.util.List;

import com.apt.data.Cell;
import com.apt.data.Domain;
import com.apt.data.Node;
import com.apt.data.RegularSquareMesh;
import com.apt.data.Spacing;

/**
 * @author SA05SF
 *
 */
public class CreateSquareMeshAndCheck {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		int nx = 8;
		int ny = 8;
		double x0 = 0.0;
		double y0 = 0.0;
		double dx = 10.0;
		double dy = 10.0;
		
		Spacing spacing = new Spacing(dx, dy);
		Domain domain = new Domain(x0, (nx-1)*dx, y0, (ny-1)*dy);
		System.err.printf("Create square mesh with domain: %s \n", domain);
		List<Node> nodes = new ArrayList<Node>();
		
		// create nodes...
		int index = 0;
		for (int j = 0; j < ny; j++) {
			double y = y0 + j*dy;
			for (int i = 0; i < nx; i++) {
				double x = x0 + i*dx;
				double z = 25.0+Math.random()*3.0;;
				Node n = new Node(++index, x, y);
				if (i < nx/2 - j)
					n.depth = 10.0;
				else
					n.depth = -z;
				System.err.printf("Create Node: %4d [%d, %d] at: (%f %f %f) \n", index, i, j, x, y, n.depth);
				nodes.add(n);
			}
		}
		
		RegularSquareMesh mesh = new RegularSquareMesh(domain,spacing, nodes);
		
		// pick some random locations within the domain and locate the cells....
		for (int k = 0; k < 10; k++) {
			double x = x0 + Math.random()*dx*(nx-1);
			double y = y0 + Math.random()*dy*(ny-1);
			
			System.err.println();
			System.err.printf("Locate cell containing: (%f, %f) ....\n", x, y);
			Cell cell = mesh.findCell(x, y, null);
		
			System.err.printf(" cell is: %s \n", cell); 
			
		}
		

	}

}

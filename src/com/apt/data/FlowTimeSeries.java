/**
 * 
 */
package com.apt.data;

import java.util.ArrayList;
import java.util.List;

/**
 * @author SA05SF
 *
 */
public class FlowTimeSeries implements IFlowTimeSeries {

	/** List of flow vectors per time-step.*/
	List<FlowVector> flows;

	private long time0;
	
	private long step;
	
	private int nstep = 0;
	
	/**
	 * 
	 */
	public FlowTimeSeries(long time0, long step) {
		this.time0 = time0;
		this.step = step;
		flows = new ArrayList<FlowVector>();
	}

	/**
	 * Create FlowTimeSeries with all flows specified.
	 * @param flows The set of flow vectors.
	 * @param time0 The start time [ms].
	 * @param step Step size [ms].
	 */
	public FlowTimeSeries(List<FlowVector> flows, long time0, long step) {
		this.time0 = time0;
		this.step = step;
		this.flows = flows;
		nstep = flows.size();
	}
	
	/** Add a FlowVector for the next time-step.*/
	public void append(FlowVector flow) {
		flows.add(flow);
		nstep++;
	}
	
	/**
	 * Fetch the flow data at a specific time. The available data is cycled ie repeated.
	 * @param time Time [ms] from start time.
	 * @return FlowVector for that time.
	 */
	@Override
	public FlowVector getFlow(long time) {
		//System.err.printf("FlowTS::getFlow:: nstep: %d time0: %d step: %d at time: %d (%f) \n", 
			//	nstep, time0, step, time, (double)time/1000.0);
		//System.err.printf("FlowTS::getFlow:: time offset: %d ms (%f s) \n", time - time0, (double)(time - time0)/1000.0);
		// Find the cycle and offset within cycle.
		double cycleLength = (double)step*(nstep - 1);
		//System.err.printf("FlowTS::getFlow:: cycleLength: %f (%f s) \n", cycleLength, (double)cycleLength/1000.0);
		int cycle = (int)Math.floor((double)(time - time0)/cycleLength);
		// start of this cycle
		double cycleStart = cycle*cycleLength; 
		// step within cycle
		int istep = (int)Math.floor(((double)(time - time0) - cycleStart)/(double)step);
		
		
		//System.err.printf("FlowTS::getFlow:: cycle: %d cstart: %f step: %d \n", cycle, cycleStart, istep);
		FlowVector flow = flows.get(istep);
		//System.err.printf("FlowTS::getFlow:: returning: %s \n", flow);
		return flow;
		
	}
	
}

package com.apt.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RegularSquareMesh implements Mesh  {

	/** Mesh domain bounds.*/
	private Domain domain;
	
	/** Square mesh spacing.*/
	private Spacing spacing;
	
	/** Cell width x [m].*/
	private double dx;
	
	/** Cell height y [m].*/
	private double dy;
	
	/** Number of cells in x.*/
	private int nx;
	
	/** Number of cells in y.*/
	private int ny;
	
	/** List of nodes in mesh.*/
	private List<Node> nodes;
	
	/** Mapping from cell index to cell.*/
	private Map<Integer, Cell> cells;
	
	/**
	 * Create a RegularSquareMesh using the supplied bounds and spacing and set of nodes.
	 * @param domain The domain bounds and spacing.
	 * @param nodes A list of nodes which define the mesh.
	 */
	public RegularSquareMesh(Domain domain, Spacing spacing, List<Node> nodes) {
		super();
		this.domain = domain;
		this.spacing = spacing;
		this.nodes = nodes;
		this.dx = spacing.dx;
		this.dy = spacing.dy;
		
		
		//for (Node n : nodes) {
			//System.err.printf("  node: %d at: (%f, %f) \n", n.ref, n.x, n.y);
		//}
		
		cells = new HashMap<Integer, Cell>();
		
		// convert list of nodes to a mesh of square cells without flow or other data
		// cells should be arranged from xmin to xmax at dx and ymin to ymax by dy so we know each cells are
		// just have to identify which nodes these are.
		nx = (int)Math.floor((domain.x1 - domain.x0)/dx);
		ny = (int)Math.floor((domain.y1 - domain.y0)/dy);
		
		int expectedCellCount = nx*ny;
		System.err.printf("RegMesh::Creating regular mesh with: domain: %s, nodes: %d cells?: %d \n", domain, nodes.size(), expectedCellCount);
	
		int icellRef = 0;
		int nCellsBy10 = expectedCellCount/10;
		// Create nx by ny cells
		for (int j = 0; j < ny; j++) {
			double y = domain.y0 + j*dy;
			for (int i = 0; i < nx; i++) {
				double x = domain.x0 + i*dx;
				// Surrounding nodes a/c wise at: x,y : x+dx,y : x+dx,y+dy : x,y+dy
				Node n1 = findNodeNearest(x,y);
				Node n2 = findNodeNearest(x+dx,y);
				Node n3 = findNodeNearest(x+dx,y+dy);
				Node n4 = findNodeNearest(x,y+dy);
				Node[] cellNodes = new Node[] {n1, n2, n3, n4};
				// found the 4 nodes round the cell
				icellRef++; // cell ref starting at 1
				Cell c = new Cell(icellRef, cellNodes);
			
				
				//if (icellRef % nCellsBy10 == 0)
				//System.err.printf("RegMesh:: Built %d of expected %d cells \n", icellRef, expectedCellCount);
				cells.put(icellRef, c);
			}
		} 
		
		System.err.printf("RegMesh:: Checking for boundary nodes...\n");
		// Decide which nodes are: land-locked, land-edge, sea-edge, internal-sea
		int nsea = 0;
		int nland = 0;
		int nopen = 0;
		for (Cell c : cells.values()) {
			int ns = 0;
			int nl = 0;
			int no = 0;
			Node[] cnodes = c.nodes;
			for (int i = 0; i < cnodes.length; i++) {
				if (isEdge(cnodes[i])) {
					if (cnodes[i].depth > 0.0) {
						cnodes[i].type = NodeType.LAND_BOUNDARY;
						nl++;
						nland++;
					} else {
						cnodes[i].type = NodeType.OPEN_BOUNDARY;
						no++;
						nopen++;
					}
					//System.err.printf("Node: %d is an edge node, depth=%f type: %s\n", cnodes[i].ref, cnodes[i].depth, cnodes[i].type.name());
				} else {
					if (cnodes[i].depth > 0.0) {
						cnodes[i].type = NodeType.LAND_BOUNDARY;
						nl++;
						nland++;
					} else {
						cnodes[i].type = NodeType.SEA;
						ns++;
						nsea++;
					}
					//System.err.printf("Node: %d is an internal node, depth=%f type: %s\n", cnodes[i].ref, cnodes[i].depth, cnodes[i].type.name());
				}
				
			}
			/*System.err.printf("Cell: %3d s.l.o:: %d.%d.%d Edges:: ", c.ref, ns, nl, no);
			for (int i = 0; i < cnodes.length; i++) {
				Node n1 = cnodes[i];
				Node n2 = cnodes[(i + 1) % cnodes.length];
				String crossing = crossingType(n1, n2);
				System.err.printf("%s.", crossing);
			}
			System.err.printf("\n");*/
			// cells on edge are either sea-edge or land-locked
			
			// cells not on edge are either land or sea
			
			
		} // next cell
		
		System.err.printf("RegMesh::Total cells: %d Nodes: %d \n", cells.size(), nodes.size());
		
	}
	
	private String crossingType(Node n1, Node n2) {
		if (n1.type == NodeType.OPEN_BOUNDARY && n2.type == NodeType.LAND_BOUNDARY)
			return "OOD";
		if (n2.type == NodeType.OPEN_BOUNDARY && n1.type == NodeType.LAND_BOUNDARY )
			return "OOD";
		if (n1.type == NodeType.OPEN_BOUNDARY && n2.type == NodeType.OPEN_BOUNDARY)
			return "OOD";
		if (n1.type == NodeType.LAND_BOUNDARY && n2.type == NodeType.LAND_BOUNDARY)
			return "LAND";
		return "SEA";
		
	}
	
	/**
	 * Determine if a node is on the edge/domain boundary.
	 * @param n The node to test.
	 * @return true if the node is an edge node.
	 */
	private boolean isEdge(Node n) {
		if (n.x == domain.x0)
			return true;
		if (n.x == domain.x1)
			return true;
		if (n.y == domain.y0)
			return true;
		if (n.y == domain.y1)
			return true;
		return false;
	}

	/** 
	 * Find the node nearest to the specified point.
	 * @param x
	 * @param y
	 * @return
	 */
	private Node findNodeNearest(double x, double y) {
		double closestDistance = 999999.999;
		Node closestNode = null;
		for (Node n : nodes) {
			double ddx = x-n.x;
			double ddy = y-n.y;
			double dist = Math.sqrt(ddx * ddx + ddy * ddy);
			if (dist < closestDistance) {
				closestDistance = dist;
				closestNode = n;
			}
		}
		//System.err.printf("findnearest: closest to: %f, %f  is:  %d at %f, %f dist: %f \n", x,y,closestNode.ref, closestNode.x, closestNode.y, closestDistance);
		return closestNode;
	}
	
	/**
	 * Find the cell containing (x,y). If no cell contains (x,y) i.e. it is outside the domain then null.
	 */
	@Override
	public Cell findCell(double x, double y, Cell lastCell) {
		// no need to use last cell but a cheap test?
		//if (lastCell != null && lastCell.contains(x, y))
			//return lastCell;
		if (x < domain.x0 || x > domain.x1)
			return null;
		if (y < domain.y0 || y > domain.y1)
			return null;
		int col = (int)Math.floor((x - domain.x0)/dx);
		int row = (int)Math.floor((y - domain.y0)/dy);
		//System.err.printf("RM::findcell: x: %f col: %d,  y: %f row: %d \n",x,col,y,row);
		
		int index = row*(nx) + col + 1;
		Cell c = cells.get(index);
		
		return c;
	}

	@Override
	public Domain getDomainBounds() {
		return domain;
	}
	
	public Spacing getSpacing() { return spacing;}

	@Override
	public List<Cell> listCells() {
		List<Cell> mycells = new ArrayList<Cell>();
		for (Cell c : cells.values()) {
			mycells.add(c);
		}
		return mycells;
	}

	
	
}

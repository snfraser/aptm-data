package com.apt.data;

public enum ChemicalComponentType {

	SOLIDS("Non-reactive solids", "NRS"), 
	CARBON_LABILE("Labile carbon", "CL"), 
	CARBON_REFACTORY("Refactory carbon", "CR"), 
	CO2("Carbon dioxide", "CO2"), 
	N2("Nitrogen", "N2"),
	TREATMENT_EMBZ("Emamectin", "EMBZ"),
	TREATMENT_TFBZ("Teflubenzarol", "TFBZ");
	
	private final String name;
	
	private final String symbol;
	
	ChemicalComponentType(String name, String symbol) {
		this.name = name;
		this.symbol = symbol;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the symbol
	 */
	public String getSymbol() {
		return symbol;
	}
	
}

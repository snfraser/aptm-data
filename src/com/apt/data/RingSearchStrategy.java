package com.apt.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.locationtech.jts.geom.GeometryFactory;

public class RingSearchStrategy implements SearchStrategy {

	/** Mapping from cell to list of cells surrounding it. */
	private Map<Integer, List<Cell>> ringMap = new HashMap<Integer, List<Cell>>();
	
	private TriangularMesh mesh;
	
	/**
	 * TODO this should happen in the prepareStragegy call not here.... ie get it from the tMesh....
	 * @param ringMap 
	 */
	public RingSearchStrategy(Map<Integer, List<Cell>> ringMap) {
		this.ringMap = ringMap;
	}

	@Override
	public void prepareStrategy(TriangularMesh mesh) {
		this.mesh = mesh;
		System.err.printf("RingSrc::prepare:: Using ringmap with %d entries\n", ringMap.size());

	}

	@Override
	public Cell performSearch(double x, double y, Cell lastCell) {
		//System.err.printf("RingSrch::ps() locate cell for: (%f, %f) starting at: %d \n", x, y, lastCell != null ? lastCell.ref: -1);
		// simple case is this the same cell?
		if (lastCell != null && lastCell.contains(x, y))
			return lastCell;
		
		// last cell is null we need to find it exhaustively..!!!!
		if (lastCell == null) {
			System.err.printf("RingSrch::ps() no start cell provided using exhaustive search...\n");
			return searchAll(x, y);
		}
		
		// start spreading out from last cell..
		//System.err.printf("RingSrch::ps() Starting search in Ring#1 ...\n");
		List<Cell> surroundingCells = ringMap.get(lastCell.ref);
		//System.err.printf("RingSrch::ps() ringmap #1 for: %d contains: %d surrounding cell refs \n", lastCell.ref, surroundingCells.size());
		
		// Search RING#1
		for (Cell sc : surroundingCells) {
			if (sc.contains(x, y))
				return sc;
		}
		System.err.printf("RingSrch::ps() ringmap #1 failed to find target point\n");
		
		// Search RING#2 - we need construct as we go along
		List<Cell> visited = new ArrayList<Cell>(); // record cells we already tested and failed
		// Visit each Ring#1 cell
		visited.add(lastCell);
		for (Cell ring1Cell : surroundingCells) {
			visited.add(ring1Cell);
		}
		
		System.err.printf("RingSrch::ps() Starting search in Ring#2 ...\n");
		for (Cell ring1Cell : surroundingCells) {
			// surrounding cells for this r1 cell
			List<Cell> ring2Cells = ringMap.get(ring1Cell.ref);
			for (Cell ring2Cell : ring2Cells) {
				System.err.printf("RingSrch::ps() ringmap #2 for: %d contains: %d surrounding cell refs \n", 
						ring1Cell.ref, ring2Cells.size());
				if (visited.contains(ring2Cell))
					continue; // skip any we already tested
				if (ring2Cell.contains(x, y))
					return ring2Cell;
				else
					visited.add(ring2Cell);
			}
		}
		
		
		// for now we just give up
		System.err.printf("RingSrch::ps() Could not find target at (%f, %f) after searching rings #1 and #2 \n", x, y);
		return null;
		
	}

	@Override
	public SearchModeType getMode() {
		return SearchModeType.RING;
	}
	
	private Cell searchAll(double x, double y) {
		
		for (Cell c : mesh.getCellList()) {
			if (c.contains(x, y))
				return c;
		}
		return null;
	}
	

}

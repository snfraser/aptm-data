package com.apt.data;


/**
 * A strategy for performing searches of a mesh.
 * 
 * @author SA05SF
 *
 */
public interface SearchStrategy {

	/** Prepare the strategy.*/
	public void prepareStrategy(TriangularMesh mesh);
	
	/** Perform a search to localize a point at (x,y).
	 * 
	 * @param x The x coordinate of the data point to localize.
	 * @param y The y coordinate of the data point to localize.
	 * @param lastCell The last cell where we may start the search from.
	 */
	public Cell performSearch(double x, double y, Cell lastCell);
	
	/**
	 * 
	 * @return The mode type of this search strategy.
	 */
	public SearchModeType getMode();
	
}

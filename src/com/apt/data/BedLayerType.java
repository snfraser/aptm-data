/**
 * 
 */
package com.apt.data;

/**
 * @author SA05SF
 *
 */
public enum BedLayerType {
	NONERODABLE, DEPOSITIONAL, BASEMENT
}

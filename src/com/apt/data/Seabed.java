/**
 * 
 */
package com.apt.data;

/**
 * 
 */
public class Seabed {
	
	private SeabedType type;
	
	private double surfaceRoughness;
	
	private double averageGrainSize;
	
	private double substrateCriticalStress;
	
	/**
	 * @param type
	 * @param substrateCriticalStress
	 * @param surfaceRoughness
	 * @param averageGrainSize
	 */
	public Seabed(SeabedType type, double substrateCriticalStress, double surfaceRoughness, double averageGrainSize) {
		super();
		this.type = type;
		this.substrateCriticalStress = substrateCriticalStress;
		this.surfaceRoughness = surfaceRoughness;
		this.averageGrainSize = averageGrainSize;
	}

	/**
	 * @return the type
	 */
	public SeabedType getType() {
		return type;
	}

	/**
	 * @return the surfaceRoughness
	 */
	public double getSurfaceRoughness() {
		return surfaceRoughness;
	}

	/**
	 * @return the averageGrainSize
	 */
	public double getAverageGrainSize() {
		return averageGrainSize;
	}

	/**
	 * @return the substrateCriticalStress
	 */
	public double getSubstrateCriticalStress() {
		return substrateCriticalStress;
	}

	@Override
	public String toString() {
		return String.format("Seabed [type=%s, surfaceRoughness=%s, averageGrainSize=%s, substrateCriticalStress=%s]",
				type, surfaceRoughness, averageGrainSize, substrateCriticalStress);
	}
	
	
	
}

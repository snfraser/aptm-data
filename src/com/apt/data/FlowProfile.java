/**
 * 
 */
package com.apt.data;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * @author SA05SF
 *
 */
public class FlowProfile implements IFlowProfile {

	List<IFlowTimeSeries> flows;
	
	List<Double> sigmas;
	
	int nsigmas = 0;

	/**
	 * 
	 */
	public FlowProfile() {
		flows = new ArrayList<IFlowTimeSeries>();
		sigmas = new ArrayList<Double>();
	}

	/**
	 * Add a flow vector, starting at seabed (layer 0).
	 * 
	 * @param flow  A flow timeseries (uvw(t)).
	 */
	public void addFlow(double sigma, IFlowTimeSeries flow) {
		System.err.printf("FlowProfile::addtimeseries at: sigma: %f \n", sigma);
		sigmas.add(sigma);
		flows.add(flow);
		nsigmas++; // count the sigma levels
	}

	public double getSigma(int level) {
		return sigmas.get(level);
	}
	
	public IFlowTimeSeries getTimeSeries(int level) {
		return flows.get(level); // could be null
	}
	
	/**
	 * Fetch flow at level sigma at time time.
	 * 
	 * @param sigma The sigma level. (Surface = 0, Seabed = -1).
	 * @param time The time [ms].
	 */
	@Override
	public FlowVector getFlow(double sigma, long time) {
		//System.err.printf("FlowProfile::getFlow(sigma: %f, time: %d) \n", sigma, time);
		
		int nsigma = flows.size(); // flows are counted from seabed as 0, 1, etc
		double sigma0 = sigmas.get(0);
		
		// check we are not below the lowest layer
		if (sigma <= sigma0) {
			//System.err.printf("FlowProfile: sigma: %f is at or below lowest measured level \n", sigma);
			double f = (1.0 + sigma)/(1.0 + sigma0);
			IFlowTimeSeries series = flows.get(0);
			FlowVector v0 = series.getFlow(time);
			double u = v0.getU(); 
			double v = v0.getV(); 
			return new FlowVector(f*u, f*v, 0.0);
		}
		
		// TODO also check if we are above the top layer..exptrapolate top 2 layers?
		
		// layer indicates the sigma level below the required sigma.
		// ie we should interpolate between (layer) and (layer + 1) if it exists.
		int layer = -1; // -1 means below lowest measurement level [0]
		for (int il = 0; il < nsigmas; il++) {
			//System.err.printf("FlowProfile::getFlow check sigma: %f against measured level: %d sigma: %f \n", sigma, il, sigmas.get(il));
			if (sigma > sigmas.get(il)) 
				layer = il;
		}
		//System.err.printf("FlowProfile: layer below sigma %f is: %d \n", sigma, layer);
		
		
		// above highest layer - extrapolate
		if (layer == nsigmas - 1) {
			IFlowTimeSeries seriesn1 = flows.get(nsigmas-1);
			FlowVector vn1 = seriesn1.getFlow(time);
			double vmn1 = vn1.getMagnitude();
			
			IFlowTimeSeries seriesn2 = flows.get(nsigmas-2);
			FlowVector vn2 = seriesn2.getFlow(time);
			double vmn2 = vn2.getMagnitude();
			
			double m = (vmn1 - vmn2 ) / (sigmas.get(nsigmas-1) - sigmas.get(nsigmas-2));
			
			double uu1 = vn1.getU();
			double vv1 = vn1.getV();
			
			return new FlowVector(uu1 + m * (sigma-sigmas.get(nsigmas-1)), vv1 + m * (sigma-sigmas.get(nsigmas-2)), 0.0);
		}
			
		// between 2 layers with known flow
		// NOTE WE SHOULD INTERPOLATE EACH COORD SEPERATELY FOR UU AND VV
		IFlowTimeSeries series1 = flows.get(layer+1);
		FlowVector f1 = series1.getFlow(time);
		double m1 = f1.getMagnitude();
		double uu1 = f1.getU(); 
		double vv1 = f1.getV(); 
		
		IFlowTimeSeries series2 = flows.get(layer);
		FlowVector f2 = series2.getFlow(time);
		double m2 = f2.getMagnitude();
		double uu2 = f2.getU(); 
		double vv2 = f2.getV(); 
		
		// double m = (m1 - m2 ) / (sigmas.get(layer+1) - sigmas.get(layer));
		
		double mu = (uu1 - uu2) / (sigmas.get(layer+1) - sigmas.get(layer));
		double mv = (vv1 - vv2) / (sigmas.get(layer+1) - sigmas.get(layer));
		
		return new FlowVector(uu1 + mu * (sigma-sigmas.get(layer+1)), vv1 + mv * (sigma-sigmas.get(layer)), 0.0);
		
	}

}

/**
 * 
 */
package com.apt.data;

/**
 * @author SA05SF
 *
 */
public enum ParticlesState {

	SUSPEND, ONBED, CONSOLIDATED, LOST

}

package com.apt.data;

import java.util.List;

public interface Mesh {

	/**
	 * Find the cell containing point (x,y) given last known cell if any.
	 * @param x X position of point.
	 * @param y Y position of point.
	 * @param lastCell last known cell location.
	 * @return The current cell location or NULL if not found.
	 */
	public Cell findCell(double x, double y, Cell lastCell);
	
	/**
	 * Get the bounds (limits) of the mesh domain.
	 * @return Mesh domain bounds.
	 */
	public Domain getDomainBounds();
	
	/**
	 * Get a list of cells in the mesh.
	 * @return A list of cells.
	 */
	public List<Cell> listCells();
}

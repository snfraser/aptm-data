package com.apt.data;

public enum CageType {
	SQUARE, CIRCULAR, SEMI_ENCLOSED_CONE, HEXAGONAL
}

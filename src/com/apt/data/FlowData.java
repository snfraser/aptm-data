/**
 * 
 */
package com.apt.data;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * The flow associated with a cell, made up from the flows at each layer.
 * To get the flow at time t and layer l use...
 * 
 * FlowVector flow = flowData.getLayer(int l).getFlowAt(int t);
 * then double u = flow.getU();
 * 
 * Where the depth and xy are not fixed to the cell grid use...???
 * we do not want to be interpolating layers to create a new layer!!!
 * 
 * instead we find the layer containing depth - no interpolation over depth
 *  FlowVector flow = flowData.findLayer(double depth).interpolateFlowAt(double time);
 *  then double u = flow.getU();
 *
 * @author SA05SF
 *
 */
public class FlowData {

	/** Set of layers mapped to their depths.*/
	List<FlowLayer> layers;
	
	/** Array to store the depths for quick access.*/
	private double[] sigmaDepths;
	

	/**
	 * Create a FlowData for a cell with an initially empty set of layers.
	 */
	public FlowData() {
		layers = new ArrayList<FlowLayer>();
		sigmaDepths = new double[50]; // 50???
	}

	/**
	 *  Add a new layer to the flow. These should be added in increasing depth order ie decreasing value
	 *  e.g:  0.0, -0.25, 0.5, -0.75, -0.95, or similar
	 *  
	 * @param layer The flow layer to add.
	 */
	public void addLayer(FlowLayer layer) {
		layers.add(layer);
		int index = layers.size()-1;
		sigmaDepths[index] = layer.getSigmaDepth();
	}
	
	public FlowLayer getLayer(int l) {
		return layers.get(l);
	}
	
	/**
	 * Find the layer containing the specified sigma depth.
	 * @param depth A sigma depth [-1.0 : 0.0]
	 * @return The layer or exception.
	 */
	public FlowLayer findLayerAt(double sigmaDepth) {
		// layers added from surface down so starts at 0.0ish
		// as soon as d > depth[i] we are done
		for (int i = 0; i < layers.size(); i++) {			
			if (sigmaDepth >= sigmaDepths[i])
					return layers.get(i);
		}
		return null;
	}
	
	/**
	 * The number of layers in the cell flow.
	 * @return
	 */
	public int getNumberLayers() {return layers.size();}
	
	

}

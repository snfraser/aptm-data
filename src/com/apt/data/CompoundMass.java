package com.apt.data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * A mass containing various chemical components. Used to represent: Feed,
 * Faeces, Deposition etc.
 */
public class CompoundMass {

	Map<ChemicalComponentType, ChemicalComponent> masses;

	/** Create a CompoundMass with an empty mass types mapping. */
	public CompoundMass() {
		this.masses = new HashMap<ChemicalComponentType, ChemicalComponent>();
	}

	/**
	 * @param masses
	 */
	public CompoundMass(Map<ChemicalComponentType, ChemicalComponent> masses) {
		this.masses = masses;
	}

	public void addComponent(ChemicalComponent component) {
		if (masses.containsKey(component.getType()))
			return;
		masses.put(component.getType(), component);
	}
	
	public List<ChemicalComponent> listComponents() {
		//for (ChemicalComponent c : masses.values())
		// TODO we may need to clone the list rather than use actual elements
		return new ArrayList(masses.values());
	}

	public void mergeCompound(CompoundMass addCompound) {
		//System.err.printf("CCM:: merge: %s \n", addCompound);
		Iterator<ChemicalComponent> components = addCompound.components();
		while (components.hasNext()) {
			// add a component mass type or add to existing components/
			ChemicalComponent c = components.next();
			//System.err.printf("CCM:: merge: %s \n", c);
			mergeComponent(c);
		}
		
	}
	
	/** Add mass to an existing component or create a new component.
	 * @param addcomponent The new component to add or merge.
	 */
	public void mergeComponent(ChemicalComponent addcomponent) {
		ChemicalComponentType type = addcomponent.getType();
		if (!masses.containsKey(type)) {
			masses.put(type, addcomponent);
		} else {
			ChemicalComponent existingMass = masses.get(type);
			masses.get(type).addMass(addcomponent.getMass());
		}
	}
	
	public void extractCompound(CompoundMass remCompound) {
		Iterator<ChemicalComponent> components = remCompound.components();
		while (components.hasNext()) {
			// add a component mass type or add to existing components/
			ChemicalComponent c = components.next();
			//System.err.printf("CCM:: merge: %s \n", c);
			extractComponent(c);
		}
		
	}
	
	public void extractComponent(ChemicalComponent removecomponent) {
		ChemicalComponentType type = removecomponent.getType();
		if (!masses.containsKey(type)) {
			//System.err.printf("CC::Extract mass: %s not found \n", type.name());
			return;
		} else {
			//System.err.printf("CC::Extract mass: %s found ", type.name());
			ChemicalComponent existingMass = masses.get(type);
			//System.err.printf(" the mass: %f ", existingMass.getMass());
			existingMass.removeMass(removecomponent.getMass());
			//System.err.printf(" -> new mass: %f \n", existingMass.getMass());
		}
	}
	
	public void extractAll() {
		Iterator components = components();
		while (components.hasNext()) {
			ChemicalComponent c = (ChemicalComponent)components.next();
			c.removeAllMass();
		}
	}

	public ChemicalComponent getComponent(ChemicalComponentType type) {
		return masses.get(type);
	}

	public double getComponentMass(ChemicalComponentType type) {
		return getComponent(type) != null ? getComponent(type).getMass() : 0.0;
	}

	public Iterator<ChemicalComponentType> types() {
		// TODO maybe make copy then return iterator....
		return masses.keySet().iterator();
	}

	public Iterator<ChemicalComponent> components() {
		// TODO maybe make copy then return iterator....
		return masses.values().iterator();
	}
	
	public ChemicalComponent copyComponentMass(ChemicalComponentType type) {
		ChemicalComponent copy = new ChemicalComponent(type, masses.get(type).getMass());
		return copy;
	}
	
	public CompoundMass copyComponentMasses() {
		CompoundMass copy = new CompoundMass();
		for (ChemicalComponent cc : masses.values()) {
			ChemicalComponent ccopy = new ChemicalComponent(cc.getType(), cc.getMass());
			copy.addComponent(ccopy);
		}
		return copy;
	}
	
	public int getCmponentCount() {return masses.size();}

	@Override
	public String toString() {
		return "CompoundMass [n_masses=" + masses.size() + "]";
	}

	
	
}

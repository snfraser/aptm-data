package com.apt.data;

import java.util.List;

public class ExhaustiveSearchStrategy implements SearchStrategy {

	List<Cell> cells;
	
	@Override
	public void prepareStrategy(TriangularMesh mesh) {
		System.err.printf("ExSrc::prepareStrategy: ncells = %d\n", mesh.getCellList().size());
		this.cells = mesh.getCellList();
	}

	@Override
	public Cell performSearch(double x, double y, Cell lastCell) {
		// we dont use the last cell in this search
		int nss = 0;
		for (Cell c : cells) {
			nss++;
			//System.err.printf("ESS::search: cell: %d at (%f, %f) \n", c.ref, c.xc, c.yc);
			if (c.contains(x,y)) {
				//System.err.printf("ESS::search: located point in cell: %d after %d\n", c.ref, nss);
				return c;	
			}
		}
		return null;
	}

	@Override
	public SearchModeType getMode() {
		return SearchModeType.EXHAUSTIVE;
	}

}
